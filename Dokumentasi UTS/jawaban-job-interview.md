# No1
**Monitoring Resource**
- RAM
1.	Untuk melihat apa yang bisa dilakukan oleh ram, seperti panduan dalam penggunaan

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor1/1.jpg)

2.	Contoh tampilan dari `free --help` seperti `free --mega` untuk melihat ram dalam size mega

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor1/2.jpg)

- CPU
1.	`ps --help all` untuk melihat apa saja yang dapat dilakukan oleh ps

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor1/3.jpg)

2.	`ps --aux` melihat proses yang sedang berjalan

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor1/4.jpg)

- Hardisk
1.	`df –help` untuk melihat df bisa ngapain aja seperti panduan

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor1/5.jpg)

2.	`df -h` untuk melihat statistik hardisk

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor1/6.jpg)


# No2
**Manajemen Program**
- memonitor program yang sedang berjalan menggunakan `top`
- menghentikan program yang sedang berjalan
1.	menggunakan command `top`

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor2/7.jpg)

2.	untuk `kill`, klik keyword k karena k singkatan dari kill

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor2/8.jpg)

Maka PID yang sedang berjalan 348022
Klik 9 untuk menghentikan program secara paksa
Klik 15 untuk menghentikan program secara defaultt
Jika stoped menjadi angka 1 maka telah berhasil membuat program berhenti
karena kita bukan owner maka menghentikan program menggunakan kill
- otomasi perintah menggunakan shell script

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor2/9.jpg)

1.	jalankan program shell script dengan `./ayg.sh` untuk mengakses file shell script nya lalu isi paramater sesuai dengan isi shell script nya.

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor2/10.jpg)

2.	Karena $4 merupakan folder dan $5 merupakan file sehingga isinya akan muncul disini berupa folder dan file

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor2/11.jpg)

- Penjadwalan eksekusi program dengan cron
1.	Ketik `crontab -e` untuk mengedit isi crontab

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor2/12.jpg)

2.	Buat perintah nya pada 1207050098-rafi disini saya membuat file baju.log dimana file ini akan muncul pada directory 1207050098-rafi dalam satu menit

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor2/13.jpg)

3.	baju.log akan muncul setelah satu menit pada halaman ini

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor2/14.jpg)


# No3
**Manajemen Network**
- Mengakses sistem operasi pada jaringan menggunakan SSH

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor3/15.jpg)

- Mampu memonitor program yang menggunakan network menggunakan netstat

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor3/16.jpg)
![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor3/17.jpg)

- Mengoperasikan HTTP Client menggunakan `curl`



# No4
**Manajemen file dan Folder**
- Navigasi
`cd`= untuk menuju ke direktori yang diinginkan
`ls`= untuk melihat semua file dan folder yang ada di direktori
`pwd`= menampilkan direktori yang sedang ditempati

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor4/19.jpg)
![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor4/20.jpg)

- CRUD file dan folder
`mkdir`= untuk membuat folder
`touch`= untuk membuat file
`rm`= untuk menghapus folder atau file
`mv`= untuk memindahkan atau me rename file atau folder
`echo`= untuk menampilkan tulisan di CLI

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor4/21.jpg)

- Manajemen ownership dan akses file dan folder
Ada dua command dasar dalam manajemen ownership yaitu `chmod` dan `chown`. 
Berikut ini adalah contoh penggunaannya: 
  `chmod [option]`
• chmod, digunakan untuk memodifikasi permission suatu file. 
• [option] berisi perintah untuk mengubah permission 
• berisi nama file yang akan diubah permissionnya.

  `chown [user:group]`
• chown, digunakan untuk memodifikasi owner dari suatu file. 
• [user:group], berisi nama user/group mana yang akan menjadi ownernya. 

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor4/22.jpg)

Pada contoh di atas file kosan dimiliki oleh owner bernama praktikumc dan group bernama praktikumc.
   Mengubah ownership dari sebuah file atau folder dilakukan dengan perintah chown <NAMA_OWNER>:<NAMA_GROUP> <NAMA_FILE_ATAU_FOLDER>
  
Mengubah owner dari folder. Arti perintah di bawah ini, file kosan ownershipnya diubah menjadi rafi dan group ownershipnya diubah menjadi uber

- Pencarian file dan folder
untuk mencari file kita bisa menggunakan command `find`

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor4/23.jpg)

- Kompresi data
1.	Buat file tar seperti dibawah ini, `kopi.tar` sebagai nama dari file tar lalu hape merupakan file yang ingin dikompres dan disimpan didalam file kopi.tar

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor4/24.jpg)

2.	Lalu akan muncul file poin.tar seperti gambar dibawah ini

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor4/25.jpg)

3.	Didalam file tar terdapat file hp yang akan di kompres

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor4/26.jpg)
![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor4/27.jpg)

4.	Lalu hapus file mall

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor4/28.jpg)

5.	Ketik xf untuk mengesktrak file nya, maka file hape akan muncul kembali setelah d ekstrak

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor4/29.jpg)

# No5
**Membuat CLI didalam shellscript**
1. saya membuat sebuat file dengan menulis command `touch  test.sh`
2. kemudian saya masuk ke editor untuk menulis command didalam shellscript `vim test.sh`

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor5/Screenshot_2023-05-10_080103.png)

3. kemudian tinggal edit sesuai yang saya inginkan kemudian hasilnya seperti ini

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor5/image_2023-05-10_080349659.png)

# No6
**Youtube**
Link : https://www.youtube.com/watch?v=TWJ-7Z4kTiA&t=240s
![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor6/image_2023-05-10_081519403.png)

# No7
**Maze Game**
Link : https://maze.informatika.digital/maze/saruman/index.html
![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor7/Screenshot__43_.png)
![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor7/Screenshot__44_.png)
![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor7/Screenshot__45_.png)
![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor7/Screenshot__46_.png)
![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor7/Screenshot__47_.png)
![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/Dokumentasi%20UTS/SS/nomor7/Screenshot__48_.png)

