import streamlit as st
import csv

def search_documents(csv_file, query):
    results = []
    with open(csv_file, 'r') as file:
        reader = csv.reader(file)
        headers = next(reader)  # Membaca baris header

        for row in reader:
            if len(row) > 4:
                word = row[1]
                explanation = row[4]

                if query.lower() in word.lower():
                    results.append({
                        'word': word,
                        'explanation': explanation
                    })

    return results

def main():
    st.title('Apartinya')

    query = st.text_input('Masukkan kata yang ingin dicari')
    submit_button = st.button('Cari')

    if submit_button:
        results = search_documents('dataset.csv', query)
        if results:
            st.header('Hasil Pencarian')
            for result in results:
                st.subheader(f"Kata: {result['word']}")
                st.write(f"Penjelasan: {result['explanation']}")
        else:
            st.info('Kata tidak ditemukan')

if __name__ == '__main__':
    main()
