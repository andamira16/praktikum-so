# Deskripsi project
Project ini dibuat untuk membantu orang berkomunikasi secara elegan dengan menggunakan kata sifat indonesia yang jarang didengar. project ini bernama Apartinya yang berarti apa artinya kata ini. project ini menggunakan library streamlit dengan bahasa python dan dataset ini diambil dari kaggle kemudian dipanggil di dalam app.py untuk menampilkan dataset kedalam webpage/webservice. copyright Rafi Andamira 2023

# Story Pembuatan
1. buat file yang dibutuhkan dengan cara touch (namafile) jangan lupa data yang diperlukan seperti dataset.csv

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/UAS/ss/1.png)

2. isi file Dockerfile dengan perintah nvim

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/UAS/ss/2.png)

`FROM debian:bullseye-slim`: Ini menentukan base image yang akan digunakan untuk membangun Docker image. Base image yang dipilih adalah Debian dengan tag bullseye-slim.

`RUN apt-get update && apt-get install -y git neovim netcat python3-pip`: Ini menjalankan perintah apt-get update untuk memperbarui daftar paket dan apt-get install untuk menginstal paket-paket yang diperlukan, yaitu git, neovim, netcat, dan python3-pip.

`COPY . /home/project-apartinya`: Ini menyalin seluruh konten dari direktori lokal ke direktori /home/project-apartinya di dalam container Docker.

`WORKDIR /home/project-apartinya`: Ini mengatur working directory di dalam container Docker menjadi /home/project-apartinya.

`RUN pip3 install --no-cache-dir -r requirements.txt`: Ini menjalankan perintah pip3 install untuk menginstal semua dependensi yang diperlukan oleh aplikasi yang tercantum dalam file requirements.txt.

`EXPOSE 24098`: Ini menentukan port yang akan diexpose di dalam container Docker. Dalam hal ini, port 24098 akan diexpose.

`CMD ["streamlit", "run", "--server.port=24098", "app.py"]`: Ini menentukan perintah default yang akan dijalankan ketika container Docker dijalankan. Perintah tersebut adalah streamlit run --server.port=24098 app.py, yang akan menjalankan aplikasi Streamlit dengan port 24098.

Dengan menggunakan Dockerfile ini, Anda dapat membangun Docker image dengan menjalankan perintah docker build dan kemudian menjalankan container Docker dari Docker image tersebut menggunakan perintah `docker-compose up -d`. 

3. isi file requirements.txt 

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/UAS/ss/3.png)

requirements.txt ini berfungsi untuk menginstall dependensi yang dibutuhkan untuk file app.py

4. isi docker-compose.yml

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/UAS/ss/4.png)

`version: "3":` Ini menentukan versi dari Docker Compose file. Dalam hal ini, menggunakan versi 3.

`services:` Ini adalah bagian di mana Anda mendefinisikan layanan (service) yang akan digunakan dalam environment Docker.

`project-apartinya:` Ini adalah nama layanan yang diberikan kepada container yang akan dibangun dari image andamirarafi/project-apartinya:1.5.

`image:` andamirarafi/project-apartinya:1.5: Ini menentukan image yang akan digunakan untuk membangun container. Dalam hal ini, menggunakan image andamirarafi/project-apartinya:1.5.

`ports:` - "24098:24098": Ini menentukan mapping port antara port host dan port container. Dalam hal ini, port 24098 akan di-mapping dari host ke port 24098 di dalam container.

`networks:` - niat-yang-suci: Ini menentukan penggunaan jaringan yang sudah ada sebelumnya dengan nama niat-yang-suci. Jaringan ini dapat di-define di luar file docker-compose.yml atau di environment Docker yang terpisah.

`volumes:` - /home/praktikumc/1207050098/project-apartinya:/home/project-apartinya: Ini menentukan mounting volume antara direktori host dengan direktori di dalam container. Dalam hal ini, direktori /home/praktikumc/1207050098/project-apartinya pada host akan di-mount ke direktori /home/project-apartinya di dalam container.

`networks`: niat-yang-suci: external: true: Ini menandakan bahwa jaringan niat-yang-suci merupakan jaringan eksternal yang sudah ada sebelumnya.

Dengan menggunakan docker-compose.yml ini, Anda dapat menjalankan environment Docker dengan menjalankan perintah docker-compose up untuk membangun dan menjalankan container sesuai dengan konfigurasi yang diberikan.

5. Build images dengan perintah dibawah

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/UAS/ss/5.png)

`docker build`: Ini adalah perintah untuk membangun Docker image.
`-t project-apartinya:1.5`: Opsi -t digunakan untuk memberikan tag (nama) pada Docker image yang akan dibangun. Dalam hal ini, Docker image akan diberi tag project-apartinya:1.5. Tag ini dapat digunakan untuk mengidentifikasi versi atau label dari Docker image.
`.`: Titik (dot) di sini menunjukkan direktori saat ini sebagai lokasi Dockerfile. Ini berarti Dockerfile yang akan digunakan untuk membangun Docker image ada di direktori saat ini.

Dengan menjalankan perintah docker build -t project-apartinya:1.5 ., Docker akan memproses Dockerfile yang ada di direktori saat ini dan membangun Docker image dengan nama project-apartinya dan tag 1.5. Setelah proses pembangunan selesai, Docker image dapat digunakan untuk menjalankan container Docker.

6. menjalankan tag untuk mengunggah ke dockerhub

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/UAS/ss/6.png)

`docker tag`: Ini adalah perintah untuk membuat tag baru untuk Docker image.
`project-apartinya:1.5`: Ini adalah nama dan tag dari Docker image yang akan diberi tag baru.
`andamirarafi/project-apartinya:1.5`: Ini adalah nama dan tag baru yang akan diberikan pada Docker image.

Dengan menjalankan perintah docker tag project-apartinya:1.5 andamirarafi/project-apartinya:1.5, Docker akan membuat tag baru andamirarafi/project-apartinya:1.5 untuk Docker image dengan nama project-apartinya dan tag 1.5. Tag baru ini bisa berguna saat ingin mengunggah atau membagikan Docker image ke repository Docker yang berbeda atau saat ingin memberikan label kustom pada Docker image.

7. Login ke docker hub

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/UAS/ss/7.png)

Diperlikan login untuk mengunggah repository images ke dockerhub

8. Push dengan perintah dibawah

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/UAS/ss/8.png)

9. Cek apakah repository sudah dibutuhkan

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/UAS/ss/9.png)

10. Create container untuk untuk bisa diakses oleh publik dengan port tertentu yang sudah di konfigurasi di Dockerfile

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/UAS/ss/10.png)

11. Buka url di web browser favorit kalian

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/UAS/ss/11.png)

12. hasilnya

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/UAS/ss/12.png)

![](https://gitlab.com/andamira16/praktikum-so/-/raw/main/UAS/ss/13.png)

# containerization helps
Secara keseluruhan, containerization mempermudah pengembangan aplikasi dengan menyediakan isolasi, portabilitas, dan konsistensi lingkungan pengembangan. Hal ini memungkinkan pengembang untuk fokus pada pengembangan aplikasi itu sendiri, sambil menjaga keandalan, konsistensi, dan skalabilitas aplikasi di berbagai lingkungan.

# apa itu DevOps, bagaimana DevOps membantu pengembangan aplikasi
DevOps merupakan pendekatan pengembangan perangkat lunak yang menggabungkan praktik-praktik dari pengembangan (Development) dan operasional (Operations) untuk menciptakan proses yang terintegrasi dan berkelanjutan dalam siklus hidup pengembangan aplikasi. DevOps bertujuan untuk meningkatkan kerjasama antara tim pengembangan dan tim operasional, sehingga dapat menghasilkan pengiriman perangkat lunak yang lebih cepat, lebih andal, dan lebih efisien.

Secara keseluruhan, DevOps membantu menghilangkan hambatan antara tim pengembangan dan tim operasional, serta mempercepat siklus pengembangan aplikasi dengan otomatisasi dan praktik terintegrasi. Dengan adopsi DevOps, organisasi dapat mencapai pengiriman perangkat lunak yang lebih cepat, lebih handal, dan lebih responsif terhadap perubahan pasar.

# penerapan DevOps di perusahaan / industri
Salah satu contoh implementasi DevOps di Gojek adalah dengan menggunakan otomatisasi dan alur kerja yang terintegrasi. Gojek menggunakan berbagai alat dan praktik untuk mempercepat pengembangan dan pengiriman aplikasi. Mereka melakukan otomatisasi dalam setiap tahap pengembangan, mulai dari membangun kode, melakukan pengujian, hingga menyebarluaskan ke lingkungan produksi. Dengan ini, mereka dapat mengurangi waktu dan usaha yang diperlukan untuk meluncurkan fitur baru atau memperbaiki bug.

Selain itu, Gojek juga mengadopsi praktik Infrastruktur sebagai Kode (IaC) untuk mengelola infrastruktur mereka. Mereka menggunakan alat seperti Terraform dan Kubernetes untuk mengotomatiskan proses penerapan dan pengelolaan infrastruktur. Hal ini memungkinkan mereka untuk dengan mudah menambahkan sumber daya, melakukan skalabilitas, dan menjaga konsistensi di lingkungan produksi.

# link webpage/webservice
http://135.181.26.148:24098

# Thanks to
Allah
